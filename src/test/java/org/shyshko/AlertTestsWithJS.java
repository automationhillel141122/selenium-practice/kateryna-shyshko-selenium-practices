package org.shyshko;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class AlertTestsWithJS {
        WebDriver driver;

        @BeforeClass
        public void beforeClass() {
            driver = new ChromeDriver();
            driver.manage().window().maximize();
        }

        @AfterClass
        public void afterClass() {
            if (driver != null) {
                driver.quit();
            }
        }
        @BeforeMethod
        public void beforeMethod() {
            driver.get("http://the-internet.herokuapp.com/javascript_alerts");
        }

        //Test1. Homework12. Alerts. Click for JS Confirm. Ok
        @Test
        public void clickForJSConfirmOk() {
            WebElement button = driver.findElement(By.xpath("//button[text()='Click for JS Confirm']"));
            button.click();
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            alert.accept();
            WebElement result =driver.findElement(By.id("result"));
            String resultText = result.getText();

            Assert.assertEquals(alertText, "I am a JS Confirm");
            Assert.assertEquals(resultText, "You clicked: Ok");
        }

        //Test2. Homework12. Alerts. Click for JS Confirm. Cansel
        @Test
        public void clickForJSConfirmCancel() {
            WebElement button = driver.findElement(By.xpath("//button[text()='Click for JS Confirm']"));
            button.click();
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            alert.dismiss();
            WebElement result =driver.findElement(By.id("result"));
            String resultText = result.getText();

            Assert.assertEquals(alertText, "I am a JS Confirm");
            Assert.assertEquals(resultText, "You clicked: Cancel");
        }

        //Test3. Homework12. Alerts. Click for JS Prompt. Ok. Text
        @Test
        public void clickForJSPromptOkText() {
            WebElement button = driver.findElement(By.xpath("//button[text()='Click for JS Prompt']"));
            button.click();
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            alert.sendKeys("123456 qwerty");
            alert.accept();
            WebElement result =driver.findElement(By.id("result"));
            String resultText = result.getText();

            Assert.assertEquals(alertText, "I am a JS prompt");
            Assert.assertEquals(resultText, "You entered: 123456 qwerty");
        }

        //Test4. Homework12. Alerts. Click for JS Prompt. Ok. Text. Empty
        @Test
        public void clickForJSPromptOkTextEmpty() {
            WebElement button = driver.findElement(By.xpath("//button[text()='Click for JS Prompt']"));
            button.click();
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            // alert.sendKeys("");
            alert.accept();
            WebElement result =driver.findElement(By.id("result"));
            String resultText = result.getText();

            Assert.assertEquals(alertText, "I am a JS prompt");
            Assert.assertEquals(resultText, "You entered:");
        }

        //Test5. Homework12. Alerts. Click for JS Prompt. Cancel. Text
        @Test
        public void clickForJSPromptCancelText() {
            WebElement button = driver.findElement(By.xpath("//button[text()='Click for JS Prompt']"));
            button.click();
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            alert.sendKeys("123 qwerty 456");
            alert.dismiss();
            WebElement result =driver.findElement(By.id("result"));
            String resultText = result.getText();

            Assert.assertEquals(alertText, "I am a JS prompt");
            Assert.assertEquals(resultText, "You entered: null");
        }

        //Test6. Homework12. Alerts. Click for JS Prompt. Cancel. Text. Empty
        @Test
        public void clickForJSPromptCancelTextEmpty() {
            WebElement button = driver.findElement(By.xpath("//button[text()='Click for JS Prompt']"));
            button.click();
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            alert.dismiss();
            WebElement result =driver.findElement(By.id("result"));
            String resultText = result.getText();

            Assert.assertEquals(alertText, "I am a JS prompt");
            Assert.assertEquals(resultText, "You entered: null");
        }

        ///////////////////////////////////////////////////////////////////

        //Test1.1. Homework13. Alerts. Click for JS Confirm. Ok// JavaScriptExecutor
        @Test
        public void clickForJSConfirmOkJS1() {
            JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
            String script = "jsConfirm()";
            javascriptExecutor.executeScript(script);

            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            alert.accept();
            WebElement result =driver.findElement(By.id("result"));
            String resultText = result.getText();

            Assert.assertEquals(alertText, "I am a JS Confirm");
            Assert.assertEquals(resultText, "You clicked: Ok");
        }

        //Test1.2. Homework13. Alerts. Click for JS Confirm. Cansel// JavaScriptExecutor
        @Test
        public void clickForJSConfirmCancelJS1() {
            JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
            String script = "jsConfirm()";
            javascriptExecutor.executeScript(script);

            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            alert.dismiss();
            WebElement result =driver.findElement(By.id("result"));
            String resultText = result.getText();

            Assert.assertEquals(alertText, "I am a JS Confirm");
            Assert.assertEquals(resultText, "You clicked: Cancel");
        }

        //Test1.3. Homework13. Alerts. Click for JS Prompt. Ok. Text// JavaScriptExecutor
        @Test
        public void clickForJSPromptOkTextJS1() {
            JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
            String script = "jsPrompt()";
            javascriptExecutor.executeScript(script);

            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            alert.sendKeys("123456 qwerty");
            alert.accept();
            WebElement result =driver.findElement(By.id("result"));
            String resultText = result.getText();

            Assert.assertEquals(alertText, "I am a JS prompt");
            Assert.assertEquals(resultText, "You entered: 123456 qwerty");
        }

        //Test1.4. Homework13. Alerts. Click for JS Prompt. Ok. Text. Empty// JavaScriptExecutor
        @Test
        public void clickForJSPromptOkTextEmptyJS1() {
            JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
            String script = "jsPrompt()";
            javascriptExecutor.executeScript(script);

            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            alert.accept();
            WebElement result =driver.findElement(By.id("result"));
            String resultText = result.getText();

            Assert.assertEquals(alertText, "I am a JS prompt");
            Assert.assertEquals(resultText, "You entered:");
        }

        //Test1.5. Homework13. Alerts. Click for JS Prompt. Cancel. Text// JavaScriptExecutor
        @Test
        public void clickForJSPromptCancelTextJS1() {
            JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
            String script = "jsPrompt()";
            javascriptExecutor.executeScript(script);

            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            alert.sendKeys("123 qwerty 456");
            alert.dismiss();
            WebElement result =driver.findElement(By.id("result"));
            String resultText = result.getText();

            Assert.assertEquals(alertText, "I am a JS prompt");
            Assert.assertEquals(resultText, "You entered: null");
        }

        //Test1.6. Homework13. Alerts. Click for JS Prompt. Cancel. Text. Empty// JavaScriptExecutor
        @Test
        public void clickForJSPromptCancelTextEmptyJS1() {
            JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
            String script = "jsPrompt()";
            javascriptExecutor.executeScript(script);

            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            alert.dismiss();
            WebElement result =driver.findElement(By.id("result"));
            String resultText = result.getText();

            Assert.assertEquals(alertText, "I am a JS prompt");
            Assert.assertEquals(resultText, "You entered: null");
        }
        ///////////////////////////////////////////////////////////////////

        //Test2.1. Homework13. Alerts. Click for JS Confirm. Ok// JavaScriptExecutor
        @Test
        public void clickForJSConfirmOkJS2() {
            WebElement button = driver.findElement(By.xpath("//button[text()='Click for JS Confirm']"));

            JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
            javascriptExecutor.executeScript("arguments[0].click();", button);


            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            alert.accept();
            WebElement result =driver.findElement(By.id("result"));
            String resultText = result.getText();

            Assert.assertEquals(alertText, "I am a JS Confirm");
            Assert.assertEquals(resultText, "You clicked: Ok");
        }

        //Test2.2. Homework13. Alerts. Click for JS Confirm. Cansel// JavaScriptExecutor
        @Test
        public void clickForJSConfirmCancelJS2() {
            WebElement button = driver.findElement(By.xpath("//button[text()='Click for JS Confirm']"));

            JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
            javascriptExecutor.executeScript("arguments[0].click();", button);


            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            alert.dismiss();
            WebElement result =driver.findElement(By.id("result"));
            String resultText = result.getText();

            Assert.assertEquals(alertText, "I am a JS Confirm");
            Assert.assertEquals(resultText, "You clicked: Cancel");
        }

        //Test2.3. Homework13. Alerts. Click for JS Prompt. Ok. Text// JavaScriptExecutor
        @Test
        public void clickForJSPromptOkTextJS2() {
            WebElement button = driver.findElement(By.xpath("//button[text()='Click for JS Prompt']"));

            JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
            javascriptExecutor.executeScript("arguments[0].click();", button);


            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            alert.sendKeys("123456 qwerty");
            alert.accept();
            WebElement result =driver.findElement(By.id("result"));
            String resultText = result.getText();

            Assert.assertEquals(alertText, "I am a JS prompt");
            Assert.assertEquals(resultText, "You entered: 123456 qwerty");
        }

        //Test2.4. Homework13. Alerts. Click for JS Prompt. Ok. Text. Empty// JavaScriptExecutor
        @Test
        public void clickForJSPromptOkTextEmptyJS2() {
            WebElement button = driver.findElement(By.xpath("//button[text()='Click for JS Prompt']"));

            JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
            javascriptExecutor.executeScript("arguments[0].click();", button);


            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            alert.accept();
            WebElement result =driver.findElement(By.id("result"));
            String resultText = result.getText();

            Assert.assertEquals(alertText, "I am a JS prompt");
            Assert.assertEquals(resultText, "You entered:");
        }

        //Test2.5. Homework13. Alerts. Click for JS Prompt. Cancel. Text// JavaScriptExecutor
        @Test
        public void clickForJSPromptCancelTextJS2() {
            WebElement button = driver.findElement(By.xpath("//button[text()='Click for JS Prompt']"));

            JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
            javascriptExecutor.executeScript("arguments[0].click();", button);


            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            alert.sendKeys("123 qwerty 456");
            alert.dismiss();
            WebElement result =driver.findElement(By.id("result"));
            String resultText = result.getText();

            Assert.assertEquals(alertText, "I am a JS prompt");
            Assert.assertEquals(resultText, "You entered: null");
        }

        //Test2.6. Homework13. Alerts. Click for JS Prompt. Cancel. Text. Empty// JavaScriptExecutor
        @Test
        public void clickForJSPromptCancelTextEmptyJS2() {
            WebElement button = driver.findElement(By.xpath("//button[text()='Click for JS Prompt']"));

            JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
            javascriptExecutor.executeScript("arguments[0].click();", button);


            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            alert.dismiss();
            WebElement result =driver.findElement(By.id("result"));
            String resultText = result.getText();

            Assert.assertEquals(alertText, "I am a JS prompt");
            Assert.assertEquals(resultText, "You entered: null");
        }
}
