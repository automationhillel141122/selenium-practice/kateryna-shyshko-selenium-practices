package org.shyshko;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.*;

public class Frames {
    WebDriver driver;

    @BeforeClass
    public void beforeClass() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @AfterClass
    public void afterClass() {
        if (driver != null) {
            driver.quit();
        }
    }

    @BeforeMethod
    public void beforeMethod() {
        driver.get("https://the-internet.herokuapp.com/nested_frames");
    }

    @Test(dataProvider = "getData")
    public void frameTestWithDataProvider(String frameName, String frameText){
            if (frameName != "frame-bottom") {
                driver.switchTo().frame("frame-top").switchTo().frame(frameName);
            } else {
                driver.switchTo().frame(frameName);
            }
        String content = driver.findElement(By.xpath("//body")).getText();
        Assert.assertEquals(content, frameText);
        driver.switchTo().defaultContent();
    }


    @DataProvider
    public Object[][] getData() {
        Object [][] data = new Object[][]{
                {"frame-middle", "MIDDLE"},
                {"frame-left", "LEFT"},
                {"frame-right", "RIGHT"},
                {"frame-bottom", "BOTTOM"}
        };
        return data;
    }
}
